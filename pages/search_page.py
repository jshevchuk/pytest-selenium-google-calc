#!/usr/bin/env python

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from config import search_url

search_field_locator = "//input[@type='text']"

class SearchPage:

    def open_search_page(driver):
        driver.get(search_url)

    def input_search_query(driver, query):
        search_field = driver.find_element(by=By.XPATH, value=search_field_locator)
        search_field.send_keys(query)

    def submit_search_query(driver):
        search_field = driver.find_element(by=By.XPATH, value=search_field_locator)
        search_field.send_keys(Keys.ENTER)

    def clear_search_field(driver):
        search_field = driver.find_element(by=By.XPATH, value=search_field_locator)
        search_field.clear()
    


