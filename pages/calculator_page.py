#!/usr/bin/env python

from selenium.webdriver.common.by import By

import config

# replace em-dash with en-dashes, if needed
calculation_sequence = config.calculation.replace('-', '−').split() if '-' in config.calculation else config.calculation.split()


class CalculatorPage():

    def calculate(driver):
        for element in calculation_sequence:
            driver.implicitly_wait(5)
            button_element = driver.find_element(by=By.XPATH, value=f'//div[text()="{element}"]')
            button_element.click()

    def access_calculation(driver):
        return driver.find_element(by=By.XPATH, value='//span[@class="vUGUtc"]')

    def access_calculation_result(driver):
        return driver.find_element(by=By.ID, value="cwos")

