import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

# fixture that initiates a WebDriver for Google Chrome
@pytest.fixture(scope='session')
def chromedriver():
    s=Service(ChromeDriverManager().install())
    driver = webdriver.Chrome(service=s)
    yield driver
    # the browser will close after running the tests
    driver.quit()
