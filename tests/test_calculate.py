#!/usr/bin/env python

import pytest
from selenium.webdriver.support.ui import WebDriverWait

import config
from pages.calculator_page import CalculatorPage
from pages.search_page import SearchPage

    
def test_chrome_calculate(chromedriver):

    SearchPage.open_search_page(chromedriver)

    # make sure that the index page has been loaded
    assert chromedriver.current_url == config.search_url

    SearchPage.input_search_query(chromedriver, config.search_query)
    SearchPage.submit_search_query(chromedriver)

    wait = WebDriverWait(chromedriver, 15)
    wait.until(lambda chromedriver: chromedriver.current_url != config.search_url)

    CalculatorPage.calculate(chromedriver)

    calculation = CalculatorPage.access_calculation(chromedriver)
    calculation_result = CalculatorPage.access_calculation_result(chromedriver)

    assert calculation.text == config.calculation
    assert calculation_result.text == config.calculation_result

    
